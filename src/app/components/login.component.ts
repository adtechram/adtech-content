import { Component} from '@angular/core';
import { Router} from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: '../templates/login.component.html',
  styleUrls: ['../style/component.style.css']
  //providers: [ROUTER_PROVIDERS]
})
export class LoginComponent {
  constructor(private router: Router) { }
  submitLogin(){
    console.log("Submitted sucessdully");
    this.router.navigateByUrl('/homePage');
  }
}
