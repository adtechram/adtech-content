import { NgModule }      from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
export const routes: Routes = [
  {path: '', redirectTo: 'loginPage', pathMatch: 'full'},    
 
];
@NgModule({
  imports: [ RouterModule.forRoot(routes,{useHash: true}) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule{ } 