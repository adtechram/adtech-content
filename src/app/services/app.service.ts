import { Injectable } from '@angular/core';
import { Http , Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import { SearchItem } from '../data/product';
import { ProductList } from '../data/promotion-list'

@Injectable()
export class ProductService {
   private _producturl="https://abhiproject-200910.appspot.com/getUserDetails";
   private _promotionurl='./assets/product.json';
   constructor(private _http: Http){}
   public loader;
   getproducts(): Observable<SearchItem[]> {
   //this.loader=true;    
    return this._http.get(this._producturl)
        .map((response: Response) => {
            
           if (response) {
                //this.loader=false;
                //console.log(this.loader);
                console.log(response.json());
                response.json().map(item => { console.log("My type", item)})
                if (response.status === 201 || response.status === 200) {
                    return response.json().map(item => {
                        console.log("my item", item)
                       
                       return new SearchItem(
                            item.id,
                            item.name,
                        );
                        
                    });
                }else if (response.status === 404) {
                    console.log("Error occuring")
                   
                }
            }
        }).catch((error: any) => {
                
                if (error.status < 400 ||  error.status ===500) {
                    return Observable.throw(new Error(error.status));
                    
                }
        }).finally(() => {
            
            console.log("Service call ended")
                //this.loader=false;
        });
         
    }
    getProductList():Observable<ProductList[]>{
        return this._http.get(this._promotionurl)
        .map((response: Response) => {
            if (response) {
                //this.loader=false;
               // console.log(this.loader);
                if (response.status === 201 || response.status === 200) {
                
                    return response.json().map(item => {
                        console.log("my response", item.imageUrl);
                        return new ProductList(
                                item.ProductId,
                                item.ProductTitle,
                                item.imageUrl,
                                item.ScreenSize,
                                item.OperatingSystem,
                                item.StorageCapacity,
                                item.Features,
                                item.ProductType,
                                item.Description,
                                item.Author,
                                item.Language,
                                item.BookType,
                                item.Founder,
                                item.Industry,
                                item.Products,
                                item.UserId,
                                item.UserName,
                                item.Headquarters
                        );
                    
                        
                        
                    });
                }else if (response.status === 404) {
                    console.log("Error occuring")
                   
                }
            }
        }).catch((error: any) => {
                
                if (error.status < 400 ||  error.status ===500) {
                    return Observable.throw(new Error(error.status));
                    
                }
        }).finally(() => {
            
            console.log("Service call ended")
              
        });        
        
    };
   
}